import os
import unittest
import pickle
import json
import datetime

from fedivis import settings, core

from ipydex import IPS, activate_ips_on_exception

activate_ips_on_exception()

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
TEST_CACHE = os.path.join(CURRENT_DIR, "data", "test_cache.pcl")
TEST_WORKDIR = os.path.join(CURRENT_DIR, "tmp")

settings.CACHE_PATH = TEST_CACHE

# noinspection PyPep8Naming
class TestCore(unittest.TestCase):
    def setUp(self):
        os.makedirs(TEST_WORKDIR, exist_ok=True)
        self.old_dir = os.getcwd()
        os.chdir(TEST_WORKDIR)

    def test_a_core1(self):
        with open(TEST_CACHE, "rb") as pfile:
            pdict = pickle.load(pfile)

    def test_b_colors(self):
        cc = core.get_color_pair_cycler()
        pair1 = next(cc)
        c1, c2 = pair1
        # self.assertNotEqual(c1, c2[:len(c1)])

    def test_c_graph1(self):

        url = "https://social.tchncs.de/@GratianRiter@bildung.social/110943047250920198"
        RGG = core.ReplyGraphGenerator()
        RGG.create_graph(url, use_cache=True)
