This directory contains tooling to publish post-graphs as svg file embedded in an html file.


Usage (on the maintainers system):
- go to a suitable directory, e.g. `_gitignore-pub`
- from there go to e.g. `img`
- create the svg file via call to `fedivis -r ...`
- from the publication dir call `py3 upload.py -r <path-to-svg>`
