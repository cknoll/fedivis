import os
import time
import argparse

from ipydex import IPS, activate_ips_on_exception, Container, set_trace
from jinja2 import Environment, PackageLoader, select_autoescape, FileSystemLoader

activate_ips_on_exception()

parser = argparse.ArgumentParser()
parser.add_argument("--render", "-r", help="svg-fname")

# TODO: read default language from the config
parser.add_argument("--lang", "-l", help="de or en", default="de")
args = parser.parse_args()

try:
    # this will be part of standard library for python >= 3.11
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib


confpath = "config.toml"

assert os.path.isfile(args.render)
src_path: str = args.render

# this path will be used inside the resulting html file
svg_fpath = src_path.replace("src/", "")
preview_img_fpath = svg_fpath.replace(".svg", "-prev.png")
preview_img_fname = os.path.split(preview_img_fpath)[-1]

assert src_path.endswith(".svg")

basepath, src_svg_fname = os.path.split(src_path)
target_html_fname = src_svg_fname.replace(".svg", ".html")

target_html_path = f"html_v/{target_html_fname}"
target_svg_dir = f"html_v/svg-2023"

lang = args.lang

# prevent unexpected strings
assert len(lang) <= 6



templatedir_path = f"../pubtools/templates"
template_path = f"graphpage-{lang}.html"


time_str = time.strftime(r"%Y-%m-%d %H:%M:%S")



with open(confpath, "rb") as fp:
    CONF = tomllib.load(fp)

user = CONF["user"]
host = CONF["host"]

target_dir = CONF["target_dir"]


target_url = f"https://{user}.uber.space/{target_dir}/{target_html_fname}"
preview_img_url = f"https://{user}.uber.space/{target_dir}/{preview_img_fpath}"

# example host: foobar.uberspace.de
upload_rsync_cmd = f"rsync -avz --progress  ./html_{target_dir}/ {user}@{host}:/home/{user}/html/{target_dir}/"



def render_html():
    context = {
        "time_str": time_str,
        "svg_fpath": svg_fpath,
        "target_url": target_url,
        "preview_img_url": preview_img_url,

    }



    jin_env = Environment(
        loader=FileSystemLoader(templatedir_path),
    )

    template_doc = jin_env.get_template(template_path)

    res = template_doc.render(context=context)
    with open(target_html_path, "w") as resfile:
        resfile.write(res)

    # copy svg file
    svg_rsync_cmd = f"rsync -az --recursive {src_path} {target_svg_dir}/"
    os.system(svg_rsync_cmd)

    convert_cmd1 = f"convert -resize 400 {target_svg_dir}/{src_svg_fname} {target_svg_dir}/{preview_img_fname}"
    # print(convert_cmd)

    if os.path.exists(f"{target_svg_dir}/{preview_img_fname}"):
        print(f"not overwriting {target_svg_dir}/{preview_img_fname}")
    else:
        os.system(convert_cmd1)

    # TODO: make the resulting image at least square or wider


render_html()

print(f"check the result at {target_url}")
os.system(upload_rsync_cmd)
