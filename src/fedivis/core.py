import os
import datetime
import re
import unicodedata
import random
import itertools as it
import collections
import xml.etree.ElementTree as ET
import json
from typing import Tuple, Union
import requests

from mastodon import Mastodon, errors
import networkx as nx

from bs4 import BeautifulSoup, Tag
import nxv
import bleach
import tabulate
import yaml

try:
    # this will be part of standard library for python >= 3.11
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib

try:
    # useful during development
    from ipydex import IPS
except ModuleNotFoundError:
    pass



from . import settings
random.seed(1702)
random.seed(1706)

AVATAR_SIZE = 48


class FedivisError(ValueError):
    pass


all_colors = [
    '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2',
    '#7f7f7f', '#bcbd22', '#17becf'
]

SHORT_DATE_FORMAT = "%m-%d"


def get_conf(confpath: str = None):
    if confpath is None:
        confpath = "config.toml"

    try:
        with open(confpath, "rb") as fp:
            CONF = tomllib.load(fp)
    except FileNotFoundError:
        print("No config file found. Using API in public mode")

        # use default value
        CONF = {
            "access_token": "pymastoclient_123_usercred.secret",
            "mastodon_url": "https://social.tchncs.de"
        }

    return CONF


def sortkey(pair):
    c1, c2 = pair

    L = len(c1)
    return (c1 != c2[:L], all_colors.index(c1), all_colors.index(c2[:L]))


def get_sorted_pairs(colors):
    colors2 = [c+"20" for c in colors]

    pairs = list(it.product(colors, colors2))

    pairs.sort(key=sortkey)
    return pairs


def get_color_pair_cycler():
    """
    get pairs for colors (edge and fill color) such that the resulting graph looks somewhat nice

    but also enable a total of 100 combinations
    """

    # 'tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple'
    # nice colors
    colors1 =  all_colors[:5]

    pairs1 = get_sorted_pairs(colors1)
    pairs2 = get_sorted_pairs(all_colors)

    # remove duplicates but maintain order
    pairs = [*pairs1]
    for p in pairs2:
        if p in pairs1:
            continue
        pairs.append(p)

    while True:
        for pair in pairs:
            yield pair

author_color_map = {}
color_pair_cycler = get_color_pair_cycler()


def get_author_colors(data):
    aid = data["account"]["id"]
    colors = author_color_map.get(aid)
    if colors:
        return colors
    else:
        # get new color from cycler, save it, return it
        colors = next(color_pair_cycler)
        author_color_map[aid] = colors
        return colors


def get_formatted_followers_count(data):

    c = data["account"].get("followers_count")
    if not c:
        return ""

    assert isinstance(c, int)
    if c < 1000:
        c_str = str(c)
    elif c < 10_000:
        c_str = f"{c/1e3:3.1f}K"
    elif c < 1000_000:
        c_str = f"{c/1e3:3.0f}K"
    elif c < 10_000_000:
        c_str = f"{c/1e6:3.1f}M"
    elif c < 1000_000_000:
        c_str = f"{c/1e6:3.0f}M"
    else:
        c_str = f"{c/1e9:3.1f}B"

    return c_str


class Node(object):
    spov_mode = False
    def __init__(self, data: dict, node_id: int, is_auxiliary=False):
        self.id = str(data["id"])
        self.order_index = node_id  # this might be changed by a function
        self.is_auxiliary = is_auxiliary
        self.node_id = node_id
        self.label = self.id
        self.data = data
        self.tags = data["tags"]
        self.title = "title"
        self.lit = None # level index tuple
        self.stripped_content = None

        # attributes to store connections for navigating through the nodes
        self.prev_horizontal = None
        self.next_horizontal = None
        self.prev_vertical = None
        self.next_vertical = None

        display_name = data["account"]["display_name"]
        if display_name:
            self.author = display_name
        else:
            self.author = data["account"]["username"]
        if self.spov_mode:
            N = 25
        else:
            N = 10
        if len(self.author) > N:
            self.author = f"{self.author[:N-2]}…"
        # label-trick: generate strings like r'{K12345}'
        # these can be substituted later -> multiple keys for line break

        sep = "\n"
        if favs := self.data.get("favourites_count"):
            star_str = f"\n{favs}__FAVS__"
            sep = " "
        else:
            star_str = ""
        if boosts := self.data.get("reblogs_count"):
            # include line break only if necessary
            boosts_str = f"{sep}{boosts}__BOOSTS__"
            sep = " "
        else:
            boosts_str = ""

        followers_str = f"{sep}{get_formatted_followers_count(self.data)}__FF__"

        if star_str and boosts_str:
            self.impact_str = f"{star_str}{boosts_str}\n{followers_str}"
        else:
            self.impact_str = f"{star_str}{boosts_str}{followers_str}"


        self.repr_str = f"{{K{self.id}}}\n{{A{self.id}}}\n{{X{self.id}}}{self.impact_str}"

        self.highlight = None
        self.decide_highlight()

        edge_color, fill_color = self.get_colors()
        self.style_dict = {
            "color": f"{edge_color}",
            "fillcolor": fill_color,  # add low alpha-value for transparency
            "style": "filled, rounded",
            "shape": "square",
            "fontname": "Open Sans",
            "fixedsize": True,
            "width": 0.7,
            "fontsize": 8,
            "peripheries": 1, # more would be possible but I was unable to assign different colors
            "penwidth": 2,
        }

        self.date_str = self.time_str = ""
        self._get_date_and_time_str()
        self.adapt_for_spov_mode()

    def decide_highlight(self):
        if self.spov_mode:
            # in spov mode highlight is handled by java script
            return False
        if hl_string := self.data.get("hl_string"):
            if hl_string.lower() in self.data["content"].lower():
                self.highlight = True
            else:
                self.highlight = False

    def get_colors(self):
        """
        return node color (either for author-based coloring or for string highlighting)
        """

        if self.is_auxiliary:
            c = "#"+"c0"*3
            return c, c

        no_hl_color = "#777777"  # medium grey
        hl_color = "#ff7f0e"  # matplotlib orange
        if self.data.get("hl_string"):
            if self.highlight:
                return hl_color, hl_color + "20"
            else:
                return no_hl_color, no_hl_color + "20"
        else:
            return get_author_colors(self.data)

    def adapt_for_spov_mode(self):
        if self.spov_mode:
            self.repr_str = f"__node{self.node_id+1}__"

    def get_stripped_content(self):
            if self.stripped_content is not None:
                return self.stripped_content


            if self.spov_mode:
                allowed_tags = ["br", "a"]
            else:
                allowed_tags = []
            self.stripped_content = bleach.clean(self.data["content"], tags=allowed_tags, strip=True)

            # "&nbsp;" caused problems once
            self.stripped_content = self.stripped_content.replace("&nbsp;", " ")
            return self.stripped_content

    def get_yaml_dict(self, node_id, reply_node_id):
        """
        :param node_id:     int, id wrt the current graph (different from the global `id`)
        """

        if reply_node_id is not None:
            reply_diff = node_id - reply_node_id
        else:
            reply_diff = None

        res = {
            "id": self.id,
            "node_id": node_id,
            "in_reply_to_id": reply_node_id,
            "reply_diff": reply_diff,
            "delimiter1": "-"*5,
            "author": f'@{self.data["account"]["acct"]}',
            "content": self.get_stripped_content(),
            "delimiter2": "-"*5,
            "tags": {
                "approval_to_parent (-10, 10)": None,
                "kindness (-10, 10)": None,
                "information_content (0, 10)": None,
                "emotion_content (0, 10)": None,
            },
            "delimiter3": "-"*20,
        }

        if reply_node_id is None:
            res["tags"].pop("approval_to_parent (-10, 10)")

        return res

    def _get_date_and_time_str(self):
        date = self.data["created_at"]

        # this is needed because in the unittest data the type had been converted
        if isinstance(date, str):
            date = datetime.datetime.fromisoformat(date)

        if date:
            self.date_str = date.strftime(r"%Y-%m-%d")
            self.time_str = date.strftime(r"%H:%M:%S")

    def __repr__(self):

        return self.repr_str


def get_root_id_from_url(url: str):

    # expect something like:
    # https://social.tchncs.de/web/@ueckueck@dresden.network/109253718049953006

    parts = url.split("/")

    root_id = parts[-1]

    return root_id


GLOBAL_REPLACEMENT_TUPLES = [
    ("__FF__", "&#128100;"),
    ("__FAVS__", "&#x2B50;"),
    # ("__BOOSTS__", "&#8613;"),
    ("__BOOSTS__", "&#x1F504;"),
    ]


# This mechanism can be used by spov for database based cache
class FediRequestCache:
    def __init__(self):
        self.url = None
        self.id = None

    def get(self, key: str, fallback: callable):
        return self.find_cached_result(key, fallback)

    def find_cached_result(self, key, fallback):
        msg = "this should be overwritten by a child class"
        raise NotImplementedError(msg)


class ReplyGraphGenerator:

    def __init__(self, spov_mode: bool = False, graphviz_bin=None):
        """
        :param spov_mode:   generate the graph tailored for the spov web application
        """
        self.root_id: str = None
        self.svg_data: bytes = None
        self.inner_svg_data: str = None
        self.viewbox: list = None
        self.nm: NodeManager = None
        self.tree_stats = None
        self.spov_mode = spov_mode
        self.mc = None  # MastodonClient

        # this is necessary to enforce custom graphviz version on uberspace
        self.graphviz_bin = graphviz_bin

        # influence how the nodes are created
        Node.spov_mode = spov_mode

    def create_graph(self, url: str, hl_string: str = None, use_cache=False, png=False):
        """
        calls _generate_svg_data and saves the result
        """

        self._generate_svg_data(url, hl_string, use_cache)

        if hl_string:
            hl_fname_appendix = f"_HL_{slugify(hl_string)}"
        else:
            hl_fname_appendix = ""

        svg_fname = f"toot-analysis-{self.root_id}{hl_fname_appendix}.svg"

        with open(svg_fname, "wb") as svgfile:
            svgfile.write(self.svg_data)
        print("File written:", svg_fname)
        if png:
            assert svg_fname[-4:] == ".svg"
            png_fname = f"{svg_fname[:-4]}.png"

            # this requires 'imagemagick' installed
            cmd = f"convert {svg_fname} {png_fname}"
            ret = os.system(cmd)
            if ret == 0:
                print("File written:", png_fname)
            else:
                print("Error during creation of", png_fname)

    def _generate_svg_data(self, url: str, hl_string: str = None, use_cache=False):
        """
        Creates the graph
        """
        self._prepare_node_manager(url=url, hl_string=hl_string, use_cache=use_cache)
        self._generate_svg_data_from_nm()

    def _wrapped_request(self, func, arg1):
        """
        execute a function and convert errors to expected format
        """
        try:
            return func(arg1)
        except errors.MastodonNotFoundError as ex:
            msg = f"Mastodon API could not find data record for url: {arg1}"
            raise FedivisError(msg, 404)

    def _handle_cached_request(self, use_cache: Union[bool, FediRequestCache], cachepath: str) -> dict:
        """
        This function handles three cases:

        - (1) file base caching
        - (2) external cache-provider (e.g. database, used by spov)
        - (3) no caching activated
        """

        # Case (1)
        if use_cache is True:
            assert cachepath is not None
            # this is useful for debugging (to prevent too much api traffic)
            import cachewrapper as cw
            self.mc = cw.CacheWrapper(self.mc)
            if os.path.isfile(cachepath):
                msg = f"using existing cache file: {cachepath}"
                print(msg)
                self.mc.load_cache(cachepath)
            else:
                msg = f"{cachepath} not found"
                print(msg)
        # Case (2)
        if isinstance(use_cache, FediRequestCache):

            def fallback():
                res: dict = self._wrapped_request(self.mc.status_context, self.root_id)
                # this dict contains "ancestors" and "descendants"
                # now retrieve date for the root node itself
                res["root_node"] = self._wrapped_request(self.mc.status, self.root_id)
                return res

            mc_res = use_cache.get(self.root_id, fallback=fallback)

        # Case (3) and (1, due to cachewrapper)
        else:
            mc_res = self._wrapped_request(self.mc.status_context, self.root_id)
            mc_res["root_node"] = self._wrapped_request(self.mc.status, self.root_id)

        # postprocessing: adapt timestamp of caching
        self.cache_metadata = mc_res.get("__cache_metadata__", {})
        if ts := self.cache_metadata.get("timestamp"):
            self.cache_metadata["timestamp_str"] = ts.strftime(r"%Y-%m-%d %H:%M:%S")
        else:
            self.cache_metadata["timestamp_str"] = None

        assert isinstance(mc_res, dict)
        return mc_res

    def _prepare_node_manager(
            self, url: str,
            hl_string: str = None,
            use_cache: Union[bool, FediRequestCache] = False,
            cachepath: str = None,
            confpath: str = None,
        ):

        CONF = get_conf(confpath=confpath)
        if use_cache == True and cachepath is None:
            cachepath = settings.CACHE_PATH


        # mc means "mastodon-client"
        if self.mc is None:
            self.mc = Mastodon(
                access_token=CONF["access_token"],
                api_base_url=CONF["mastodon_url"],
            )

        if not url.startswith(CONF["mastodon_url"]):
            # the provided url does not belong to our instance
            # -> determine corresponding URL on our instance
            search_res = self.mc.search(url)["statuses"]
            if "/deck/@" in url:
                url = url.replace("/deck/@", "/@")
                search_res = self.mc.search(url)["statuses"]

            if len(search_res) == 0:
                # this is triggered for a url like [1] (copied from a logged in session of another instance)
                # [1] https://social.tchncs.de/deck/@ReproducibiliTeaGlobal@scicomm.xyz/112551561595360270
                # for non-logged in access it should return a 302 redirect response
                try:
                    direct_res =  requests.get(url, allow_redirects=False)
                except requests.exceptions.ConnectionError as ex:
                    # endow the exception with useful information (status and failing url)
                    ex.args = (*ex.args, 404, url)
                    raise ex

                if direct_res.is_redirect:
                    new_url = direct_res.headers["Location"]
                    # call this method again
                    return self._prepare_node_manager(
                        url = new_url,
                        hl_string = hl_string,
                        use_cache = use_cache,
                        cachepath = cachepath,
                        confpath = confpath ,
                    )
                msg = f"unexpected response for url: {url} (expected redirect but got status code {direct_res.status_code})"
                raise FedivisError(msg, 500)

            if len(search_res) > 1:
                msg = f"unexpected length of search results: {len(search_res)}"
                raise FedivisError(msg, 500)
            self.root_id = str(search_res[0]["id"])
        else:
            self.root_id = get_root_id_from_url(url)

        mc_res = self._handle_cached_request(use_cache=use_cache, cachepath=cachepath)
        self.nm = NodeManager(
            root_id=self.root_id,
            root_node_data=mc_res["root_node"],
            hl_string=hl_string,
            spov_mode=self.spov_mode
        )

        MAX_NODES = 4000
        if len(mc_res["descendants"]) > MAX_NODES:
            msg = f"The requested graph contains more than {MAX_NODES} nodes and is thus too big to be visualized."
            raise FedivisError(msg, 500)

        self.nm.extend_graph_from_status_list(parent=self.nm.root_node, descendants=mc_res["descendants"])

        # +1 to account for the root node
        # (ommited nodes are those whose parent has been deleted and which are thus disconnected from the graph)
        assert len(self.nm.G) == len(mc_res["descendants"]) - self.nm.omitted_nodes + 1
        if self.spov_mode:
            self.nm.optionally_introduce_additional_level1()

        if use_cache == True:
            # this is for the file-based cache
            self.mc.save_cache(cachepath)

    def _generate_svg_data_from_nm(self):

        # <title>node0009</title>
        old_title_str_template = "<title>node{:04d}xx</title>"
        new_title_str_template = "<title>{}</title>"

        # generate replacement tuples
        rpl_tuples = []
        node: Node
        for node in self.nm.G.nodes:
            # todo: add + 1 here?
            s1 = old_title_str_template.format(node.node_id)


            title_content = f"{node.date_str} {node.time_str}\n{node.get_stripped_content()}"
            s2 = new_title_str_template.format(title_content)
            rpl_tuples.append((s1, s2))

        # define style for graph visualization

        # see https://nxv.readthedocs.io/en/latest/reference.html#styling
        style = nxv.Style(
            graph={"rankdir": "TB"},
            node=lambda u, d: u.style_dict,
                # u is a node and d is its attribute dict (which is ignored here)
            edge=lambda u, v, d: {"style": "solid", "arrowType": "normal", "label": ""},
        )

        # create the raw SVG of the graph

        self.raw_svg_data = nxv.render(self.nm.G, style, format="svg", graphviz_bin=self.graphviz_bin)

        if self.spov_mode:
            self._post_process_raw_svg_for_avatarmode()
        else:
            self._post_process_raw_svg_for_textmode(rpl_tuples)

        assert self.svg_data is not None
        self.create_inner_svg()
        self.root_id = self.nm.root_id

        if not self.spov_mode:
            # print some rough status info on the cli
            N = len(self.nm.G)
            print(f"{N} nodes processed")

    def _get_new_clip_path(self, node_id_str, x, y):
        """
        create something like
        <clipPath id="cp_node5">
            <rect x="74" y="-300" width="38" height="38" rx="10" ry="10" />
        </clipPath>
        """
        s = AVATAR_SIZE
        r = "11"
        clippath = Tag(name="clipPath", attrs={"id": f"cp_{node_id_str}"})
        rect = Tag(name="rect", attrs={"x": x, "y": y, "width": s,  "height": s, "rx": r,  "ry": r})
        clippath.append(rect)
        return clippath

    def _post_process_raw_svg_for_avatarmode(self) -> None:

        self.svg_data = self.raw_svg_data.decode("utf8")

        self.bs = BeautifulSoup(self.svg_data, 'xml')

        svg_obj = self.bs.find("svg")
        self.defs = Tag(name="defs")
        svg_obj.insert(0, self.defs)
        self.defs.append("\n")

        # new_xml_snippet = """<new_element>This is a new element</new_element>"""
        # new_tag = BeautifulSoup(new_xml_snippet, 'xml').new_element

        self.__process_g_elements()
        self.svg_data = self.bs.encode("utf8")

    def __process_g_elements(self):

        g_elements = get_element_dict_by_id(self.bs, "g")

        for node_id, node in self.nm.node_id_map.items():
            node_id_str = f"node{node_id + 1}"

            g_element = g_elements[node_id_str]
            if not self.spov_mode and node.highlight:
                # in spov mode this is handled by javascript
                g_element["class"] = f'{g_element["class"]} node_highlight'
            path = g_element.find("path")
            path.attrs["stroke-width"] = 3
            path.attrs["fill-opacity"] = 0.7

            txt_element = g_element.find("text")
            x, y = float(txt_element["x"]), float(txt_element["y"])
            s = AVATAR_SIZE
            d = AVATAR_SIZE/2

            xp = x - d
            yp = y - d*1.08
            img_url = node.data["account"]["avatar_static"]
            attrs = {"clip-path": f"url(#cp_{node_id_str})"}
            img_tag = Tag(name="image", attrs=attrs)
            img_tag["xlink:href"] = img_url
            img_tag["x"] = xp
            img_tag["y"] = yp
            img_tag["width"] = s
            img_tag["height"] = s
            self.defs.append(self._get_new_clip_path(node_id_str, xp, yp))
            self.defs.append("\n")

            g_element.insert(0, img_tag)
            txt_element.decompose()

    def _post_process_raw_svg_for_textmode(self, rpl_tuples) -> None:

        entity_links = []

        for node in self.nm.G.nodes.keys():
            date = node.data["created_at"]

            # this is needed because in the unittest data the type had been converted
            if isinstance(date, str):
                date = datetime.datetime.fromisoformat(date)

            if date:
                short_date_time_str = f'{date.strftime(SHORT_DATE_FORMAT)} {date.strftime("%H:%M")}'
                str1 = f'<a href="{node.data["url"]}" target="_blank">{short_date_time_str}</a>'
                str2 = f'<a href="{node.data["url"]}" target="_blank">{len(node.get_stripped_content())}C</a>'
            else:
                str1 = str2 = ""

            entity_links.append((f"A{node.id}", bleach.clean(node.author)))
            entity_links.append((f"K{node.id}", str1))
            entity_links.append((f"X{node.id}", str2))

        # insert links to wiki data urls
        self.raw_svg_data = self.raw_svg_data.decode("utf8").format(**dict(entity_links))

        # this looks like inefficient but can be tolerated as it is not used in spov mode
        import time
        t1 = time.time()
        for s1, s2 in rpl_tuples + GLOBAL_REPLACEMENT_TUPLES:
            self.raw_svg_data = self.raw_svg_data.replace(s1, s2)
        dt = time.time() - t1
        # print(f"{dt=}")

        self.svg_data = self.raw_svg_data.encode("utf8")

    def create_inner_svg(self):
        """
        create self.inner_svg_data as str for embedding in html
        """
        bs = BeautifulSoup(self.svg_data, 'xml')
        self.inner_svg_data = bs.find("svg").decode()
        self.calc_viewbox()

        g_elements = get_element_dict_by_id(bs, "g")

        # add relevant data as json (to make it accessible from js)
        node: Node
        for node in self.nm.G.nodes():
            node_id = node.node_id
            node_id_str = f"node{node_id + 1}"
            g_element = g_elements[node_id_str]

            node_data = {
                "date_str": node.date_str,
                "time_str": node.time_str,
                "author": node.author,
                "url": node.data["url"],
                "author_avatar": node.data["account"]["avatar_static"],
                "impact_str": node.impact_str,
                "content": node.stripped_content,
                "is_auxiliary": node.is_auxiliary,
                "node_id": node.node_id,  # this is to ensure consistency between svg id and python id (not trivial)
            }
            g_element["node-data"] = json.dumps(node_data)

        # convert the DOM back to string representation
        self.inner_svg_data = bs.decode()

        for s1, s2 in GLOBAL_REPLACEMENT_TUPLES:
            self.inner_svg_data = self.inner_svg_data.replace(s1, s2)

    def get_tree_structure_connectors(self):
        """
        For navigating through the tree: find horizontal and vertical connections.

        This function uses the "Level Index Tuple" (short: lit).

        It is a tuple of non-negative ints with the following meaning
            - (0,) is the root node.
            - (0, 0) is the first child of the root node (counting "naturally", i.e. starting with 1)
            - (0, 2, 5) is the 6th child of the 3rd child of the root node etc.
        """
        # this sets n.lit for each node
        self.nm.prepare_tree_structure_connectors()

        # connect horizontally
        for level in self.nm.levels:
            assert len(level) > 0

            # originally we used node.order_index -> sometimes strange orders
            # now we use node.lit
            level.sort(key=lambda j: self.nm.node_id_map[j].lit)

            # connect 0 <-> 1, 1 <-> 2, etc
            for node_id1, node_id2 in zip(level[:-1], level[1:]):
                self._connect_nodes(node_id1, node_id2, "horizontal")

            # connect last with first
            self._connect_nodes(level[-1], level[0], "horizontal")

        # connect vertically
        # sort leaves according to (maybe changed) structure
        leaves = sorted(self.nm.leaves.values(), key=lambda n: n.lit)
        for leave_node in leaves:
            self._connect_up(leave_node)

        bs = BeautifulSoup(self.inner_svg_data, 'xml')
        g_elements = get_element_dict_by_id(bs, "g")

        for node in self.nm.G.nodes.keys():
            node_id = node.node_id
            node_id_str = f"node{node_id + 1}"
            g_element = g_elements[node_id_str]

            connection_data = {
                "prev_horizontal": node.prev_horizontal,
                "next_horizontal": node.next_horizontal,
                "next_vertical": node.next_vertical,
                "prev_vertical": node.prev_vertical,
            }

            g_element["node-connections"] = json.dumps(connection_data)

        self.inner_svg_data = bs.decode()

    def _connect_up(self, node: Node):
        """
        Recursive function to vertically connect the current node with its direct parent.
        End recursion once an existing connection is found or at root level.
        """
        predecessors: Tuple[Node] = tuple(self.nm.G.predecessors(node))

        if len(predecessors) == 0:
            return

        assert len(predecessors) == 1

        self._connect_nodes(predecessors[0].node_id, node.node_id, "vertical")
        if predecessors[0].next_vertical != f"node{node.node_id + 1}":
            # this connection has not been set (due to earlier existing)
            # do not go further up the tree (towards the root)
            return
        else:
            self._connect_up(predecessors[0])

    def _connect_nodes(self, node_id1: int, node_id2: int, mode: str):
        """
        Save horizontal and vertical connections as node attributes.
        Existing vertical connections are not overwritten
        """
        node1: Node = self.nm.node_id_map[node_id1]
        node2: Node = self.nm.node_id_map[node_id2]

        if mode == "horizontal":
            node1.next_horizontal = f"node{node2.node_id + 1}"
            node2.prev_horizontal = f"node{node1.node_id + 1}"
        elif mode == "vertical":
            if node1.next_vertical is None:
                node1.next_vertical = f"node{node2.node_id + 1}"
            if node2.prev_vertical is None:
                node2.prev_vertical = f"node{node1.node_id + 1}"
        else:
            msg = f"unknown mode: {mode}"
            raise ValueError(mode)

    def calc_viewbox(self):

        # Load the SVG file
        tree0 = ET.fromstring(self.svg_data.decode("utf8"))
        tree = ET.fromstring(self.inner_svg_data)

        svg_node = next(tree.iter())

        viewbox_str = svg_node.attrib.get("viewBox")
        self.viewbox =[float(x) for x in viewbox_str.split(" ")]

    def create_tree_stats(self, save=True):

        self.tree_stats = TreeStats()
        node: Node
        for node in self.nm.G:
            self.tree_stats.total_messages += 1
            user = User.make_user(node.data["account"])
            self.tree_stats.message_count[user] += 1
            self.tree_stats.char_count[user] += len(node.get_stripped_content())
            for tag_dict in node.tags:
                tag = f'#{tag_dict["name"]}'
                self.tree_stats.hashtag_count[tag] += 1

        self.tree_stats.total_users = len(User.cache)
        if self.nm.trunk_node is not None:
            self.tree_stats.total_users -= 1
            self.tree_stats.total_messages -= 1

        if save:
            self.tree_stats.report(Nmax=10)
            report_fname = f"toot-report-{self.root_id}.txt"
            self.tree_stats.report(fpath=report_fname)

    def export_to_yaml(self):
        node: Node
        res = []
        for node_id, node in enumerate(self.nm.G):
            res.append(self.nm.get_yaml_dict(node, node_id))

        yaml_fpath = f"toot-graph-{self.root_id}.yaml"

        with open(yaml_fpath, "w") as myfile:
            yaml.safe_dump(res, myfile, allow_unicode=True, sort_keys=False)

class User:
    cache = {}
    def __init__(self, account_dict: dict):
        self.id = account_dict["id"]
        self.acct = account_dict["acct"]
        self.displayname = account_dict.get("display_name")
        self.followers_count = account_dict.get("followers_count")

    def __repr__(self):
        return f"@{self.acct} ({self.followers_count}F)"

    @staticmethod
    def init_cache():
        """
        Reset the cache to an empty dict.
        """
        User.cache = {}

    @staticmethod
    def make_user(account_dict: dict):
        id = account_dict["id"]
        existing_user = User.cache.get(id)
        if existing_user is not None:
            return existing_user
        user = User(account_dict)
        User.cache[id] = user
        return user


def get_element_dict_by_id(bs_obj: BeautifulSoup, tag_name: str) -> dict:

    elements = {}
    for element in bs_obj.find_all(tag_name):
            id_str = element.get("id")
            if id_str is not None:
                elements[id_str] = element

    return elements


class TreeStats:
    """
    For every user count
        - number of messages
        - number of total characters

    Also: offer possibilities for sorting and reporting
    """
    def __init__(self):

        self.total_messages = 0
        self.total_users = 0
        self.message_count = collections.defaultdict(lambda: 0)
        self.char_count = collections.defaultdict(lambda: 0)
        self.hashtag_count = collections.defaultdict(lambda: 0)

        User.init_cache()


    def report(self, Nmax=None, fpath=None):
        tab = tabulate.tabulate
        lines = []
        lines.append("message count stats")
        lines.append(tab(sort_items(self.message_count.items(), reverse=True)[:Nmax], showindex=True))
        lines.append("\n")
        lines.append("character count stats")
        lines.append(tab(sort_items(self.char_count.items(), reverse=True)[:Nmax], showindex=True))
        lines.append(f"sum of all posts: {sum_items(self.char_count.items())}")

        lines.append("\n")
        lines.append("hashtag stats")
        lines.append(tab(sort_items(self.hashtag_count.items(), reverse=True)[:Nmax], showindex=True))

        lines.append("\n")
        lines.append(self.follower_report(Nmax=Nmax))

        res_str = "\n".join(lines)
        if fpath:
            with open(fpath, "w") as fp:
                fp.write(res_str)
            print(f"File written: {fpath}")
        else:
            print(res_str)

    def follower_report(self, Nmax=None) -> list:
        account_objs = self.message_count.keys()
        account_followers = []
        aobj: User
        for aobj in account_objs:
            account_followers.append(
                (aobj.acct, aobj.followers_count, self.message_count[aobj], self.char_count[aobj])
            )

        headers = ("account", "followers", "msg count", "char count")
        return tabulate.tabulate(sort_items(account_followers, reverse=True)[:Nmax], showindex=True, headers=headers)

    def hashtag_report(self):
        pass


def sort_items(items, reverse=False):
    items_list = list(items)
    items_list.sort(key=lambda tup: tup[1], reverse=reverse)
    return items_list

def sum_items(items):
    items_list = list(items)
    return sum(list(zip(*items_list))[1])


class NodeManager:
    def __init__(self, root_id, root_node_data: dict, hl_string: str, spov_mode: bool):
        """
        :param root_id:     id of root node
        :param hl_string:   highlight string (optional)
        """
        self.id_map = {}
        self.G = nx.DiGraph()
        self.root_id = root_id
        self.hl_string = hl_string
        self.leaves = {}
        self.node_counter = 1
        self.spov_mode = spov_mode
        self.omitted_nodes = 0

        self.root_id = root_id
        root_node_data["hl_string"] = hl_string
        self.root_node = Node(data=root_node_data, node_id=0)

        # will be set if necessary
        self.trunk_node: Node = None

        self.id_map[root_id] = self.root_node
        self.node_id_map = {self.root_node.node_id: self.root_node}

        # will map level index tuples to node ids, see below
        # (currently not used, might be useful to sort leaves, but current sorting seems fine)
        self.lit_node_id_map = {}

        # node_id is wrt the graph, whereas id is the global id
        # self.id_to__node_id_map = {}  # todo: obsolete?

        # nested list of node ids, e.g. [[0], [1, 3], [2]]
        # meaning that 1 and 3 are on level 1, i.e. direct children of 0 (root node, level 0)
        # and 2 is on level 2
        self.levels = []

        self.G.add_node(self.root_node)

    def extend_graph_from_status_list(self, parent, descendants, limit=300):
        """
        :param: parent:         parent node (from self.G)
        :param: descendants:    list of statuses from res["descendants"]
        """

        for node_id, obj_data in enumerate(descendants, start=1):
            obj_data["hl_string"] = self.hl_string
            node = Node(obj_data, node_id=node_id)

            # this uses the mastodon id
            self.id_map[node.id] = node

            # this uses our internal id
            self.node_id_map[node.node_id] = node

            try:
                parent = self.id_map[str(node.data["in_reply_to_id"])]
            except KeyError:
                # omit Nodes without parent (probably they have been deleted)
                self.omitted_nodes +=1
                continue

            # keep track of the leaves (nodes with no children)
            self.leaves.pop(parent.id, None)
            self.leaves[node.id] = node

            self.G.add_node(node)
            self.G.add_edge(parent, node)
            self.node_counter += 1

    def optionally_introduce_additional_level1(self):
        """
        For better overview we want to add an additional node (trunk_node) at level one.
        It should be displayed directly under the root node.
        The trunk_node should be parent of all nodes with at least one child.
        Those level1-nodes without any child should be direct descendants of the root node
        """

        self.prepare_tree_structure_connectors()
        if len(self.levels) < 2:
            # very short tree, nothing todo
            return

        original_level_1_nodes = self.levels[1]

        level1_leaves = []
        level1_nodes_with_children = []

        for node_id in original_level_1_nodes:
            node = self.node_id_map[node_id]
            successors_iter = self.G.successors(node)
            try:
                next(successors_iter)
            except StopIteration:
                # that node has no successors
                level1_leaves.append(node)
            else:
                level1_nodes_with_children.append(node)

        if len(level1_leaves) < 3 or len(level1_nodes_with_children) < 3:
            return


        trunk_node_text = (
            "This tree-node does not correspond to a real post. "
            "However, it serves to achieve a more place-efficient arrangement of the nodes."
        )
        data = {
            "id": "__trunk__",
            "tags": [],
            "content": trunk_node_text,
            "url": "",
            "account": {
                "display_name": "spov system", "id": 0, "avatar_static": "", "acct": {},
            },
            "created_at": datetime.datetime.now()
        }
        self.trunk_node = Node(data, self.node_counter, is_auxiliary=True)
        self.node_counter += 1
        self.node_id_map[self.trunk_node.node_id] = self.trunk_node

        level1_lit_list = []
        for j in self.levels[1]:
            if self.node_id_map[j].is_auxiliary:
                continue
            level1_lit_list.append(self.node_id_map[j].lit)
        #  [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5)]

        # get the second arguments
        elements1 = list(zip(*level1_lit_list))[1]
        virtual_index = elements1[len(elements1)//2] - 0.5

        # this is for horizontal connection
        self.trunk_node.lit = (0, virtual_index)
        # this is for graphviz
        self.trunk_node.order_index = level1_leaves[len(level1_leaves)//2].order_index - 0.5

        # trunk_node.order_index = 0

        self.G.add_node(self.trunk_node)
        self.G.add_edge(self.root_node, self.trunk_node)

        for node in level1_nodes_with_children:
            self.G.remove_edge(self.root_node, node)
            self.G.add_edge(self.trunk_node, node)

        old_G = self.G
        new_G = nx.DiGraph(node_prefix='my_node_')
        new_G.add_nodes_from(sorted(self.G.nodes(data=True), key=lambda n: n[0].lit))
        new_G.add_edges_from(self.G.edges(data=True))
        self.G = new_G
        del old_G

        # renumber all nodes to be consistent with the uninfluencible node id created by the svg renderer
        self.node_id_map.clear()
        node: Node
        for i, node in enumerate(self.G.nodes.keys()):

            old_id = node.node_id
            node.node_id = i
            self.node_id_map[i] = node

    def get_yaml_dict(self, node: Node, node_id: int):

        reply_id = node.data["in_reply_to_id"]

        # get the graph-related node_id of the node for which this is an reply

        if rn := self.id_map.get(str(reply_id)):
            reply_node_id = rn.node_id
        else:
            reply_node_id = None
        return node.get_yaml_dict(node_id, reply_node_id)

    def prepare_tree_structure_connectors(self):
        """
        Fill the following data structures.
        self.levels,
        self.lit_node_id_map

        This method might be called several times because the structure of the graph might be
        manipulated for better displayability
        """

        self.levels = []
        self.lit_node_id_map = {}


        def key_func(node: Node) -> Tuple[int]:
            lit = node.lit
            if lit is None:
                lit = ()
            return lit

        def process_level(level: int, node_ids: Tuple[int], lit: Tuple[int]):
            """
            Recursive function which goes through the graph and fills some data structures.
            :param level:       int; distance from the root node
            :param node_ids:    Tuple[int]; sequence of node ids to process
            :param lit:         Tuple[int]; means: level index tuple


            The lit is a tuple of non-negative ints with the following meaning
                - (0,) is the root node.
                - (0, 0) is the first child of the root node (counting "naturally", i.e. starting with 1)
                - (0, 2, 5) is the 6th child of the 3rd child of the root node etc.
            """

            # if there is something to add create a new level-list
            if len(self.levels) == level and node_ids:
                self.levels.append([])

            for idx, node_id in enumerate(node_ids):
                # lit: level index-tuple
                new_lit = lit + (idx,)
                self.lit_node_id_map[new_lit] = node_id
                node = self.node_id_map[node_id]
                node.lit = new_lit
                self.levels[level].append(node.node_id)

                successors = sorted(self.G.successors(node), key=key_func)

                successor_ids = [n.node_id for n in successors]
                process_level(level=level+1, node_ids=successor_ids, lit=new_lit)

        # this processes all levels via recursion
        process_level(level=0, node_ids=[self.root_node.node_id], lit=())


# based on https://stackoverflow.com/a/295466
def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')


def script_main():

    print("Script successfully executed")
